<?php

/**
 * @file
 * Helper functions for hook_block().
 */

/**
 * Helper function for hook_block : lists all widget blocks.
 */
function _uwa_widget_block_list() {
  $blocks = array();

  $results = db_query('SELECT wid,name FROM {uwa_widgets}');
  while ($widget = db_fetch_object($results)) {
    $blocks[$widget->wid] = array(
      'info' => t('UWA Widget: @name', array('@name' => $widget->name)),
      'cache' => BLOCK_CACHE_PER_USER,
    );
  }

  return $blocks;
}

/**
 * Helper function for hook_block : builds a widget block.
 */
function _uwa_widget_block_view($delta) {
  if (is_numeric($delta)) {
    $widget = uwa_widget_load($delta);
    $block = array(
      'subject' => ''
    );
    module_load_include('inc', 'uwa_widget', 'uwa_widget.preferences');

    // Merge widget's default and provided default (last are priority)
    $preferences = array_merge($widget->default_preferences, uwa_widget_user_preferences($delta));

    _uwa_widget_obfuscate_passwords($preferences);

    $block['content'] = '';
    $can_edit_prefs = uwa_widget_preferences_access($widget);
    if ($can_edit_prefs && !$widget->configuration['header']) {
      $block['content'] .= l('edit', 'uwa_widget/preferences/'. $delta, array('query' => array('destination' => $_GET['q'])));
    }

    $query = array(
      'id' => $delta,
      'uwaUrl' => urlencode($widget->url),
      'ifproxyUrl' => urlencode(url(drupal_get_path('module', 'uwa_widget') .'/uwa_widget_proxy.html', array('absolute' => TRUE))),
    );

    // If widget's header is displayed, do not display a block title
    if ($widget->configuration['header']) {
      $query['header'] = '1';
    }
    else {
      $block['subject'] = $widget->name;
    }

    foreach ($preferences as $key => $value) {
      $query[$key] = $value;
    }

    $query_strings = array();
    foreach ($query as $name => $value) {
      $query_strings[] = $name .'='. $value;
    }
    // Putting an entire query string avoids to escape it with urlencode (as passwords "obfuscation" already did it)
    $iframe_url_options['query'] = implode('&', $query_strings);

    $iframe_url = url('http://nvmodules.netvibes.com/widget/frame/', $iframe_url_options);
    $iframe_messaging_js_url = 'http://www.netvibes.com/js/UWA/Utils/IFrameMessaging.js';

    drupal_set_html_head('<script type="text/javascript" src="'. $iframe_messaging_js_url .'"></script>');
    drupal_add_js(drupal_get_path('module', 'uwa_widget') .'/uwa_widget.js', 'module', 'header');

    $js_settings = array('uwa_widget' => array('widgets' => array($delta => array(
      'savePrefs' => $can_edit_prefs,
    ))));
    drupal_add_js($js_settings, 'setting');

    $size = '';

    if (is_numeric($widget->configuration['width'])) {
      $size .= 'width="'. $widget->configuration['width'] .'"';
    }

    $block['content'] .= '
<iframe id="frame_'. $delta .'" scrolling="no" frameborder="0" '. $size .'
  src="'. $iframe_url .'">
</iframe>';

    return $block;
  }
}

/**
 * Helper function for hook_block : builds a widget block's configure form.
 */
function _uwa_widget_block_configure($delta) {
  $form = array();
  $widget = uwa_widget_load($delta);
  if (!$widget) {
    return;
  }

  $form['uwa_widget_configuration'] = _uwa_widget_configuration_form($widget);
  $form['uwa_widget_configuration']['#title'] = t('Widget configuration');
  $form['uwa_widget_configuration']['#type'] = 'fieldset';
  $form['uwa_widget_configuration']['#collapsible'] = TRUE;
  $form['uwa_widget_configuration']['#tree'] = TRUE;

  module_load_include('inc', 'uwa_widget', 'uwa_widget.preferences');
  if ($preferences_form = _uwa_widget_preferences_form($delta)) {
    $form['uwa_widget_preferences'] = $preferences_form;
    $form['uwa_widget_preferences']['#title'] = t('Widget default preferences');
    $form['uwa_widget_preferences']['#type'] = 'fieldset';
    $form['uwa_widget_preferences']['#collapsible'] = TRUE;
    $form['uwa_widget_preferences']['#tree'] = TRUE;
  }

  return $form;
}

/**
 * Helper function for hook_block : saves a widget block configuration.
 */
function _uwa_widget_block_save($delta, $edit) {
  $preferences = $edit['uwa_widget_preferences'];
  if (is_array($preferences['uwa_other'])) {
    $preferences += $preferences['uwa_other'];
  }
  unset($preferences['uwa_other']);
  unset($preferences['uwa_hidden']);
  db_query("UPDATE {uwa_widgets} SET default_preferences = '%s', configuration = '%s' WHERE wid = %d", serialize($preferences), serialize($edit['uwa_widget_configuration']), $delta);
}

/**
 * Helper function generating the "configuration" part of a widget preferences form.
 * @param $widget the widget to edit.
 */
function _uwa_widget_configuration_form($widget) {
  $form = array();

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Force width'),
    '#default_value' => $widget->configuration['width'],
  );

  $form['header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show header'),
    '#default_value' => $widget->configuration['header'],
  );

  $t_args = array('@clean-preferences-url' => url('admin/uwa_widgets/clean/'. $widget->wid, array('query' => array('destination' => url('admin/build/block/configure/uwa_widget/'. $widget->wid)))));
  $form['user_preferences'] = array(
    '#type' => 'checkbox',
    '#title' => t('Users with permissions can edit their own preferences for this widget'),
    '#default_value' => $widget->configuration['user_preferences'],
    '#description' => t("If you uncheck this, existing user preferences will remain and be used. You can <a href=\"@clean-preferences-url\">clean this widget's users preferences</a>.", $t_args),
  );

  return $form;
}
