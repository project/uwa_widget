<?php

/**
 * @file
 * Administration pages callbacks.
 */

/**
 * Administration page. Lists all declared widgets.
 */
function uwa_widget_admin() {
  $rows = array();
  $results = db_query('SELECT wid,name,url FROM {uwa_widgets} ORDER BY name');
  while ($widget = db_fetch_object($results)) {
    $rows[] = array(
      $widget->name,
      l($widget->url, $widget->url),
      l(t('edit'), 'admin/build/uwa_widget/widgets/'. $widget->wid .'/edit'),
      l(t('delete'), 'admin/build/uwa_widget/widgets/'. $widget->wid .'/delete'),
    );
  }

  if (count($rows)) {
    $header = array(
      t('Name'),
      t('URL'),
      t('edit'),
      t('delete'),
    );

    $output = theme('table', $header, $rows, $attributes, $caption);
  }
  else {
    $output = t('Sorry, no widget was found.') .' '. l(t('Create one ?'), 'admin/build/uwa_widget/widgets/add');
  }
  return $output;
}

/**
 * Generates the widget add/edit form.
 */
function uwa_widget_form(&$form_state, $widget = NULL) {
  $form = array();


  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
  );
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['#redirect'] = 'admin/build/uwa_widget/widgets/list';

  if (is_object($widget)) {
    $form['name']['#default_value'] = $widget->name;
    $form['url']['#default_value'] = $widget->url;
    $form['wid'] = array(
      '#type' => 'value',
      '#value' => $widget->wid,
    );
  }
  return $form;
}

/**
 *
 * Widget add/edit form validate : check the URL availability.
 */
function uwa_widget_form_validate($form, &$form_state) {
  if ($form_state['values']['url']) {
    $response = drupal_http_request($form_state['values']['url']);
    switch ($response->code) {
      case 200: // HTTP OK
      case 302: // HTTP Found
      case 307: // HTTP Temporary redirect
        // Response is good, do nothing
        break;
      default:
        form_set_error('url', t('This URL is malformed or unavailable.'));
        break;
    }
  }
}

/**
 * Widget add/edit form submit : create/save the widget.
 */
function uwa_widget_form_submit($form, &$form_state) {
  $widget = array(
    'name' => $form_state['values']['name'],
    'url' => $form_state['values']['url'],
  );
  $update = NULL; // Insert record.
  if ($form_state['values']['wid']) {
    $widget['wid'] = $form_state['values']['wid'];
    $update = 'wid'; // Update record.
  }
  drupal_write_record('uwa_widgets', $widget, $update);
}

/**
 * Generates the widget deletion confirm form.
 * @param $form_state
 * @param $widget the widget to delete.
 */
function uwa_widget_delete_confirm(&$form_state, $widget) {
  $form = array();

  $form['wid'] = array(
    '#type' => 'value',
    '#value' => $widget->wid,
  );

  $form['name'] = array(
    '#type' => 'value',
    '#value' => $widget->name,
  );

  $description = t("Are you sure you want to delete the widget %name ? You can't undo this action.", array('%name' => $widget->name));

  return confirm_form($form,
    t('Delete widget %name ?', array('%name' => $widget->name)),
    'admin/build/uwa_widget/widgets/list',
    $description,
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Widget deletion confirm form submit.
 */
function  uwa_widget_delete_confirm_submit($form, &$form_state) {
  uwa_widget_delete($form_state['values']['wid']);
  drupal_set_message(t('The widget %name was deleted.', array('%name' => $form_state['values']['name'])));
  $form_state['redirect'] = 'admin/build/uwa_widget/widgets/list';
}