Drupal.behaviors.uwa_widget = function() {
  if (!Drupal.settings.uwa_widget.initialized) {
    msgHandler = function(message) {
      var id = message.id;
      switch (message.action) {
        case 'resizeHeight':
          var frame = document.getElementById('frame_' + id);
          if (frame) {
            frame.setAttribute('height', message.value);
          }
          break;
        case 'setTitle':
          var chrome = document.getElementById('widget-' + id);
          if (chrome) {
            var title = chrome.getElementsByClassName("title")[0];
            if (title) {
               title.innerHTML = message.value;
            }
          }
          break;
        case 'setValue':
          if (Drupal.settings.uwa_widget.widgets[message.id].savePrefs) {
            $.post(Drupal.settings.basePath +'uwa_widget/ajax_prefs', {
              wid : message.id,
              name : message.name,
              value : message.value
            }, function (data) {
            }, 'json');
          }
          else {
            if (!Drupal.settings.uwa_widget.widgets[message.id].alerted) {
              Drupal.settings.uwa_widget.widgets[message.id].alerted = true;
              alert(Drupal.t("Your changes on this widget won't be saved, either because the administrator chose not to save them, or because you don't have the permission."));
            }
          }
          break;
        default:
          break;
      }
    };
     
    UWA.MessageHandler = new UWA.iFrameMessaging;
    UWA.MessageHandler.init({
      'eventHandler': msgHandler,
      'trustedOrigin' : 'nvmodules.netvibes.com'
    });
    Drupal.settings.uwa_widget.initialized = true;
  }
};