<?php

/**
 * @file
 * Helper functions to generate preferences form.
 */

/**
 * Generate a widget's preferences form using form API.
 * @param $wid : the widget's id.
 */
function _uwa_widget_preferences_form($wid, $defaults = array()) {
  // get the widget's UWA file
  $widget = uwa_widget_load($wid);

  // Merge widget's default and provded default (last are priority)
  $defaults = array_merge($widget->default_preferences, $defaults);

  $preferences_list = _uwa_widget_rebuild_preferences_list($wid, $defaults);

  $form = array();
  foreach (element_children($preferences_list) as $field_name) {
    $field = $preferences_list[$field_name];
    $function = '_uwa_widget_preference_'. $field['type'];
    if (!function_exists($function)) {
      $function = '_uwa_widget_preference_default';
    }
    $form += call_user_func($function, $field, $defaults[$field['name']]);
  }
  if (count($preferences_list['#other'])) {
    $form['uwa_other'] = array(
      '#type' => 'fieldset',
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
      '#title' => t('Other settings'),
      '#description' => t('These settings are not declared by the widget, but were set by the widget. Modify them carefully.'),
    );
    foreach ($preferences_list['#other'] as $name => $field) {
      $form['uwa_other'] += _uwa_widget_preference_default($field, $defaults[$name]);
    }
  }
  if (count($preferences_list['#hidden'])) {
    $form['uwa_hidden'] = array(
      '#type' => 'fieldset',
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
      '#title' => t('Hidden variables'),
      '#description' => t('These variables are hidden by the widget, you can only see their value.'),
    );
    foreach ($preferences_list['#hidden'] as $name => $field) {
      $form['uwa_hidden'] += _uwa_widget_preference_hidden($field, $defaults[$name]);
    }
  }

  return $form;
}

/**
 * Gets a list of te preference fields of a widget, by calling a URL from
 * Netvibes. It returns a JSON describing all preferences of a widget.
 * @param $wid the widget's id.
 * @param $variables variables we can find in a user's preferences. Usually,
 * they coincide with what should return this function, but sometimes, there
 * are more variables than preferences declared by the widget.
 */
function _uwa_widget_rebuild_preferences_list($wid, $variables = array()) {
  $widget = uwa_widget_load($wid);
  $options = array(
    'query' => array(
      'uwaUrl' => $widget->url,
    )
  );

  // We concatenate a query string to this URL, to avoid Netvibes caching.
  // We use current date, formatted YmdH so that new version is asked every hour only
  if (strpos($widget->url, '?')) {
    $options['query']['uwaUrl'] .= '&';
  }
  else {
    $options['query']['uwaUrl'] .= '?';
  }
  $options['query']['uwaUrl'] .= 'n'. date('YmdH');

  // this HTTP call gets a json describing some of the widget's properties (such as its preferences)
  $response = drupal_http_request(url('http://nvmodules.netvibes.com/widget/json', $options));
  $json_data = json_decode($response->data);

  // Get previously set preferences list
  $serialized_prefs = db_result(db_query("SELECT preferences_list FROM {uwa_widgets} WHERE wid = %d", $wid));
  $old_prefs = unserialize($serialized_prefs);

  $preferences = array();
  $preferences['#hidden'] = array();
  foreach ($json_data->preferences as $field) {
    if ($field->type == 'hidden') {
      $preferences['#hidden'][$field->name] = (array)$field;
    }
    else {
      $preferences[$field->name] = (array)$field;
    }
    unset($variables[$field->name]);
  }


  // as "other" preferences, keep previous ones, and add any new
  if (is_array($old_prefs['#other'])) {
    $preferences['#other'] = $old_prefs['#other'];
  }
  else {
    $preferences['#other'] = array();
  }

  foreach ($variables as $name => $value) {
    $preferences['#other'][$name] = array(
      'type' => 'default',
      'label' => $name,
      'name' => $name,
    );
  }
  db_query("UPDATE {uwa_widgets} SET preferences_list = '%s' WHERE wid = %d", serialize($preferences), $wid);

  return $preferences;
}

/**
 * A widget's user preferences editing form.
 */
function uwa_widget_user_preferences_form($form_state, $widget) {
  global $user;

  $defaults = uwa_widget_user_preferences($widget->wid);
  if ($form['preferences'] = _uwa_widget_preferences_form($widget->wid, $defaults)) {
    $form['preferences']['#tree'] = TRUE;
    $form['update'] = array(
      '#type' => 'value',
      '#value' => FALSE,
    );

    if ($defaults) {
      $form['update']['#value'] = TRUE;
    }

    $form['wid'] = array(
      '#type' => 'value',
      '#value' => $widget->wid,
    );

    $form['buttons'] = array();
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    $form['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset to defaults'),
    );
  }
  else {
    drupal_set_message("At the moment, this widget has no settings.");
  }

  return $form;
}

/**
 * Widget's user preferences editing form submit function.
 */
function uwa_widget_user_preferences_form_submit($form, &$form_state) {
  global $user;
  $widget = uwa_widget_load($form_state['values']['wid']);
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == t('Reset to defaults')) {
    db_query('DELETE FROM {uwa_widgets_preferences} WHERE wid = %d AND uid  = %d', $form_state['values']['wid'], $user->uid);
    drupal_set_message(t('Your preferences for the widget %widget have been reset.', array('%widget' => $widget->configuration['title'])));
  }
  else {
    if ($form_state['values']['update']) {
      $update = array('wid', 'uid');
    }

    $preferences = $form_state['values']['preferences'];
    if (is_array($preferences['uwa_other'])) {
      $preferences += $preferences['uwa_other'];
    }
    unset($preferences['uwa_other']);
    unset($preferences['uwa_hidden']);
    $object = array(
      'wid' => $form_state['values']['wid'],
      'uid' => $user->uid,
      'preferences' => serialize($preferences),
    );
    drupal_write_record('uwa_widgets_preferences', $object, $update);
    drupal_set_message(t('Your preferences for the widget %widget have been saved.', array('%widget' => $widget->configuration['title'])));
  }
}

/**
 * Get unserialized user preferences for a widget.
 * @param $wid a widget id.
 * @param $uid a user id.
 */
function uwa_widget_user_preferences($wid, $uid = NULL) {
  if (!$uid) {
    global $user;
    $uid = $user->uid;
  }
  $user_prefs = unserialize(db_result(db_query('SELECT preferences FROM {uwa_widgets_preferences} WHERE wid = %d AND uid = %d', $wid, $uid)));
  if (!$user_prefs) {
    $user_prefs = array();
  }
  return $user_prefs;
}

/**
 * Preferences cleaning confirmation form.
 */
function uwa_widget_clean_confirm($form_state, $widget) {
  $form = array();

  $form['wid'] = array(
    '#type' => 'value',
    '#value' => $widget->wid,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to clean all users preferences for widget %widget ?', array('%widget' => $widget->name)),
    referer_uri(),
    t('The widget will always use its default settings instead.'),
    t('Clean'),
    t('Cancel')
  );
}

/**
 * Preferences cleaning confirmation form submit.
 */
function uwa_widget_clean_confirm_submit($form, $form_state) {
  db_query('DELETE FROM {uwa_widgets_preferences} WHERE wid = %d', $form_state['values']['wid']);
  $widget = uwa_widget_load($form_state['values']['wid']);
  drupal_set_message(t("The user preferences for widget %widget were cleaned.", array('%widget' => $widget->name)));
}

/**
 * Generates a preferences form element for UWA "list" type.
 * @param $uwa_field a UWA preference field.
 * @param $default a default value.
 */
function _uwa_widget_preference_list($uwa_field, $default = NULL) {
  $form = array();
  $form[$uwa_field['name']] = array(
    '#type' => 'select',
    '#title' => $uwa_field['label'],
    '#options' => array(),
    '#default_value' => $default !== NULL ? $default : $uwa_field['defaultValue'],
  );
  foreach ($uwa_field['options'] as $option) {
    $form[$uwa_field['name']]['#options'][$option->value] = $option->label;
  }
  return $form;
}


/**
 * Generates a preferences form element for UWA "hidden" type.
 * @param $uwa_field a UWA preference field.
 * @param $default a default value.
 */
function _uwa_widget_preference_hidden($uwa_field, $default = NULL) {
  $form = array();
  $form[$uwa_field['name']] = array(
    '#title' => $uwa_field['label'] ? $uwa_field['label'] : $uwa_field['name'],
    '#type' => 'textfield',
    '#default_value' => $default !== NULL ? $default : $uwa_field['defaultValue'],
    '#disabled' => TRUE,
  );
  return $form;
}

/**
 * Generates a preferences form element for UWA "boolean" type.
 * @param $uwa_field a UWA preference field.
 * @param $default a default value.
 */
function _uwa_widget_preference_boolean($uwa_field, $default = NULL) {
  $form = array();
  $form[$uwa_field['name']] = array(
    '#type' => 'checkbox',
    '#title' => $uwa_field['label'],
  );
  if (is_null($default)) {
    $default = $uwa_field['defaultValue'];
  }
  if (preg_match('/^false$/i', $default)) {
    $default = FALSE;
  }
  $form[$uwa_field['name']]['#default_value'] = $default;
  return $form;
}

/**
 * Generates a preferences form element for UWA "range" type.
 * @param $uwa_field a UWA preference field.
 * @param $default a default value.
 */
function _uwa_widget_preference_range($uwa_field, $default = NULL) {
  $form = array();
  $form[$uwa_field['name']] = array(
    '#type' => 'select',
    '#title' => $uwa_field['label'],
    '#options' => array(),
    '#default_value' => $default !== NULL ? $default : $uwa_field['defaultValue'],
  );
  for ($i = $uwa_field['min']; $i <= $uwa_field['max']; $i += $uwa_field['step']) {
    $form[$uwa_field['name']]['#options'][$i] = $i;
  }
  return $form;
}

/**
 * Generates a preferences form element for UWA "text" type.
 * @param $uwa_field a UWA preference field.
 * @param $default a default value.
 */
function _uwa_widget_preference_text($uwa_field, $default = NULL) {
  $form = array();
  $form[$uwa_field['name']] = array(
    '#type' => 'textfield',
    '#title' => $uwa_field['label'],
    '#default_value' => $default !== NULL ? $default : $uwa_field['defaultValue'],
  );
  return $form;
}

/**
 * Default preferences form element constructor (for unknown types).
 * @param $uwa_field a UWA preference field.
 * @param $default a default value.
 */
function _uwa_widget_preference_default($uwa_field, $default = NULL) {
  $form = array();
  $form[$uwa_field['name']] = array(
    '#type' => 'textfield',
    '#title' => $uwa_field['label'] ? $uwa_field['label'] : $uwa_field['name'],
    '#default_value' => $default != NULL ? $default : '',
  );
  return $form;
}

/**
 * Generates a preferences form element for UWA "password" type.
 * @param $uwa_field a UWA preference field.
 * @param $default a default value.
 */
function _uwa_widget_preference_password($uwa_field, $default = NULL) {
  $form = array();
  $form[$uwa_field['name']] = array(
    '#type' => 'password',
    '#title' => $uwa_field['label'],
    '#default_value' => $default !== NULL ? $default : $uwa_field['defaultValue'],
  );
  return $form;
}

