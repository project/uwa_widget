The file uwa_widget_proxy.html is used by Netvibes' UWA iFrame method :

"In order for the widget's height to be correctly resized when its content gets
displayed (and other widget communication matters), you must add a reference to
a local proxy file. This file must be hosted on the same domain as the page
where the IFrame is set."

See http://dev.netvibes.com/doc/uwa/howto/uwa_iframe .